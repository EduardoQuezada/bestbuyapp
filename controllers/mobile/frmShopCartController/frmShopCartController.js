checkNewItems = true;

define({ 

  onNavigate:function(){

    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderLocator.btnMenu.onClick=openMenu.bind(this);
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.getCartItems();
    this.closeAppMenu();



  },resumeApp:function(){

    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  } ,

  closeAppMenu:function(){


    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";


  },checkItems:function(){




    const itemsData = shoppingCart.map(item => {
      const itemsInfo = {};

      itemsInfo.new = item.new_item;

      if (itemsInfo.new == 'true' && checkNewItems == true){
        this.view.flxNew.skin="sknNew";
        this.view.lblNew.skin="sknNewItem";
        this.view.lblNew.text="You have Items that are New. Shipping may be delayed.";
        checkNewItems=false;

      }

      if (itemsInfo.new == 'false' && checkNewItems == true){

        this.view.flxNew.skin="sknNoNew";
        this.view.lblNew.skin="sknNewNormal";
        this.view.lblNew.text="Normal Shipping Schedule.";
        checkNewItems=false;


      }


      return itemsInfo;
    });



    this.resumeNavigation();
    kony.application.dismissLoadingScreen();



  },



  getCartItems:function(){

    if(totalCartValue==0 ){

      this.view.flxEmpty.isVisible=true;
      this.view.flxItems.isVisible=false;
      this.view.flxNew.isVisible=false;
      this.view.flxTotal.isVisible=false;
    }

    else{



      this.view.lblTotal.text=`${currency.format(totalCartValue)}`;


      this.view.flxItems.isVisible=true;
      this.view.flxNew.isVisible=true;
      this.view.flxEmpty.isVisible=false;
      this.view.flxTotal.isVisible=true;



      const itemsData = shoppingCart.map(item => {
        const itemsInfo = {};


        itemsInfo.display_price = item.display_price;
        itemsInfo.price = item.price.text;
        itemsInfo.desc = item.title;
        itemsInfo.new=item.new_item;
        itemsInfo.remove={onClick:this.removeAnimation};
        itemsInfo.img="cartremoveitem.png";

        if (itemsInfo.new == 'true' && checkNewItems == true){
          this.view.flxNew.skin="sknNew";
          this.view.lblNew.skin="sknNewItem";
          this.view.lblNew.text="You have Items that are New. Shipping may be delayed.";
          checkNewItems=false;
        }


        return itemsInfo;
      });



      this.view.segCartItems.widgetDataMap = {lblDesc : "desc", lblPrice: "display_price",btnRemove:"remove",imgItem:"img"};


      this.view.segCartItems.setData(itemsData);



    }


  },checkPlatform:function(){
    if (kony.os.deviceInfo().name =='iphone' || kony.os.deviceInfo().name =='android'){

    }else{

      this.removeItem();
    }


  },

  removeItem:function(){

    for (var i=0; i<this.view.segCartItems.selectedItems.length; i++){
      var selected_price= this.view.segCartItems.selectedItems[i].price;
      totalCartValue -=  parseFloat(selected_price )  ;
     
    } 
    
  

    this.view.lblTotal.text=`${currency.format(totalCartValue)}`;




    if (kony.os.deviceInfo().name =='iphone' || kony.os.deviceInfo().name =='android'){

      shoppingCart.splice(this.view.segCartItems.selectedIndices[0][1][0],1);
      this.view.segCartItems.removeAt(this.view.segCartItems.selectedIndices[0][1][0],0);

    }else{

      this.view.segCartItems.removeAt(this.view.segCartItems.selectedIndex[1],this.view.segCartItems.selectedIndex[0]);

    }


    if(totalCartValue==0 ){

      this.view.flxEmpty.isVisible=true;
      this.view.flxItems.isVisible=false;
      this.view.flxNew.isVisible=false;
      this.view.flxTotal.isVisible=false;

    }

    checkNewItems=true;
    this.checkItems();


  },rotateValue:function(){

    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate3D(90, 0, 1, 0);
    this.view.lblTotal.animate(
      kony.ui.createAnimation({
        "100": {
          "anchorPoint": {
            "x": 0.5,
            "y": 0.5
          },
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          "transform": trans100
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_BOTH,
        "duration": 1,
        "direction": kony.anim.DIRECTION_NONE
      }, {
        "animationEnd": this.rotateBack
      });
  },rotateBack:function(){

    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate3D(-10, 0, 1, 0);
    this.view.lblTotal.animate(
      kony.ui.createAnimation({
        "100": {
          "anchorPoint": {
            "x": 0.5,
            "y": 0.5
          },
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          "transform": trans100
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
      }, {
        "animationEnd": this.checkPlatform
      });

  },removeAnimation:function(){


    var self = this;


    self.rotateValue();
    this.pauseNavigation();
    showLoader();

    var scaleRow = kony.ui.makeAffineTransform();
    scaleRow.translate(0, 0);
    scaleRow.scale(0, 0);

    var animationObject = kony.ui.createAnimation({


      "100": {"height":"0dp","width":"100%",
              "stepConfig": {
                "timingFunction": kony.anim.EASE
              },
              "transform": scaleRow
             },


    });


    var animationConfig = {
      duration : 1,
      fillMode : kony.anim.FILL_MODE_FORWARDS

    };

    var animationCallbacks ={
      "animationEnd": function(){
        self.removeItem();

      }};
    var animationDefObject ={

      definition : animationObject,
      config : animationConfig,
      callbacks : animationCallbacks
    };

    var row={

      sectionIndex : 0,
      rowIndex : this.view.segCartItems.selectedIndices[0][1][0]
    };


    self.view.segCartItems.animateRows({

      rows : [row],
      widgets : ["flxCartItem"],
      animation : animationDefObject

    });



  }



});