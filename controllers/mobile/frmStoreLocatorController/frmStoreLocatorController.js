
define({ 

  onNavigate:function(){

    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderLocator.btnMenu.onClick=openMenu.bind(this);
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.view.btnSearch.onClick=this.searchLocation.bind(this);
    this.view.txtInput.onDone=this.searchLocation.bind(this);
    this.view.storeLocation.onPinClick=this.onPinClickCallback;
    this.view.storeLocation.onClick=this.hidePopUp;
    this.firstLocation();
    this.closeAppMenu();


  },closeAppMenu:function(){


    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";


  },

  hidePopUp:function(){

    this.view.flxLocation.isVisible = false;
  },
  firstLocation:function(){

    var initialPosition = {
      lat: "38.6628538",
      lon: "-101.9789502",

    };

    this.view.storeLocation.locationData=[];
    this.view.storeLocation.navigateToLocation(initialPosition,false,false);
    this.view.storeLocation.zoomLevel=5;
    this.view.txtInput.text="";
  },

  searchLocation:function(){

    var userInput = this.view.txtInput.text.trim();

    var specialChars =  /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;



    if (userInput=='' || userInput==undefined){

      showError('Invalid Entry','Please type a city name to search locations.');
    }

    else{

      if(specialChars.test(userInput)){

        showError("Invalid Keywords","Special caracters are not allowed.");
      } 

      else {

        operationName =  "getStores";
        data= {"city": userInput};
        headers= {};
        integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);

      }




    }
  },

  operationSuccess:function(res){


    if (res.stores[0]=='' || res.stores[0]==undefined){

      showError('No Stores Found','We could not find any stores in this city');

    }else{


      var Data = res.stores;
      this.view.storeLocation.locationData=[]; 

      const pinsData = Data.map(item => {
        const pinsInfo = {};


        pinsInfo.name = item.name;
        pinsInfo.hours = item.hours;
        pinsInfo.address= item.address;
        pinsInfo.lat= item.lat;
        pinsInfo.lng= item.lng;


        return pinsInfo;
      });


      const filteredLocations = pinsData.filter(
        item => item.lat!==''  && item.lng!==''

      );

      var locationList=[];
      var locations={};

      
      if (filteredLocations==''){
        
          showError('No Stores Found','We could not find any stores in this city');
      }
      
      else{
        
        
         for(var i=0 ; i<filteredLocations.length;i++){
        locations={	
          "lat":filteredLocations[i].lat,
          "lon":filteredLocations[i].lng,
          "id":i,
          "name":filteredLocations[i].name,
          "address":filteredLocations[i].address,
          "hours":(filteredLocations[i].hours=='' || filteredLocations[i].hours==undefined) ? "N/A" :filteredLocations[i].hours,
          "showCallout":false

        };

        this.view.storeLocation.addPin(locations);



        locationList.push(locations);

      }



      this.view.storeLocation.zoomLevel=10;
      this.view.storeLocation.navigateToLocation(locationList[0],false,false); 
        
      }
    

    }

  },
  operationFailure:function(res){
    alert("Could not run Service");
  },
  onPinClickCallback:function(mapid, locationdata) {


    this.view.lblLocationName.text=locationdata.name;
    this.view.lblAddress.text=locationdata.address;
    this.view.lblHours.text=locationdata.hours;
    this.view.flxLocation.isVisible=true;
  }

});