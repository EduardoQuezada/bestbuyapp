define({ 

  onNavigate:function(category){

    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderBack.btnMenu.onClick=openMenu.bind(this);
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.view.HeaderBack.btnBack.onClick=this.goBackHome;
    this.view.btnNext.onClick=this.increasePage;
    this.view.btnBefore.onClick=this.decreasePage;
    this.view.segProductList.onRowClick=this.productClicked;
    this.setRowAnimations();
    this.getProducts();
    this.closeAppMenu();


  },closeAppMenu:function(){


    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";


  },

  productClicked:function(){
    for (var i=0; i<this.view.segProductList.selectedItems.length; i++){

      this.pauseNavigation();
      showLoader();

      var ntf = new kony.mvc.Navigation('frmProductsDetail');

      ntf.navigate(this.view.segProductList.selectedItems[i]);


    } 

  },
  goBackHome:function(){
    var ntf = new kony.mvc.Navigation('frmHome');
    ntf.navigate();
  },

  getProducts:function(){

    this.pauseNavigation();
    showLoader();

    var page=productPage;
    var query=queryValue;


    if (inputSearch==true){

      operationName =  "productSearch";
      data= {"query": query,"page": page};
      headers= {};
      integrationObj.invokeOperation(operationName, headers, data, operationSuccess.bind(this), operationFailure.bind(this));


    }else{

      operationName =  "productsbyCat";
      data= {"category": categorySelected,"page": page};
      headers= {};
      integrationObj.invokeOperation(operationName, headers, data, operationSuccess.bind(this), operationFailure.bind(this));


    }


    function operationSuccess(res){

      if (res.products=='' || res.products==undefined){

        showError("No Products Found",`We're sorry we could find any products that match: ${searchValue}`);
        this.view.lblCrumbs.text="No Products Found";
        this.view.flxList.isVisible=false;
        this.view.flxNavigation.isVisible=false;

        this.resumeNavigation();
        kony.application.dismissLoadingScreen();

      }else{



        this.view.btnBefore.isVisible = (page==1) ? false:true;
        this.view.btnNext.isVisible = (page==res.total_pages) ? false:true; 
        this.view.flxList.isVisible=true;
        this.view.flxNavigation.isVisible=true;

        this.view.lblCrumbs.text = (inputSearch==true) ? `Results for: ${searchValue}`:`Category: ${searchValue}`;
        this.view.lblPage.text=`Page ${res.current_page} of ${res.total_pages}`;

        var Data = res.products;

        const productsData = Data.map(item => {
          const productsInfo = {};

          productsInfo.title = item.name;
          productsInfo.price = {
            text:(item.onSale=='true') ? item.salePrice : item.regularPrice,
            skin:(item.onSale == 'true') ? "sknOnSale":"sknNotSale"};

          productsInfo.display_price ={
            text:(item.onSale=='true') ? `$ ${item.salePrice }`: `$ ${item.regularPrice}`,
            skin:(item.onSale == 'true') ? "sknOnSale":"sknNotSale"};

          productsInfo.sale = item.onSale;
          productsInfo.new = {isVisible:(item.new == 'true') ? true:false};
          productsInfo.new_item_label={text:"!!! NEW ITEM !!!",skin:"sknYellow"};
          productsInfo.new_item = item.new;
          productsInfo.sku=item.sku;
          productsInfo.desc=item.shortDescription;
          productsInfo.review = (item.customerReviewAverage=='' || item.customerReviewAverage==undefined ||  item.customerReviewAverage==0 ) ? `Avg User Rating: N/A` :`Avg User Rating: ${item.customerReviewAverage}`;
          productsInfo.review_average=item.customerReviewAverage;
          productsInfo.reviewCount=item.customerReviewCount;

          if(item.images=='' || item.images==undefined){

            productsInfo.image= 'no_image.png';

          }else{


            productsInfo.image= (inputSearch==true) ? item.images[1].href : item.image;
            productsInfo.image_list=item.images;
          }




          return productsInfo;
        });



        this.view.segProductList.widgetDataMap = {lblTitle : "title", lblPrice: "price", lblRating : "review", imgProduct:"image",flxTop:"new",lblNew:"new_item_label"};
        this.view.segProductList.setData(productsData);




        this.resumeNavigation();
        kony.application.dismissLoadingScreen();


      }




    }


    function operationFailure(res){
      alert("Failure on API " + res);
    }
  },

  increasePage:function(){
    this.pauseNavigation();
    showLoader();

    productPage+=1;
    this.getProducts();
  },

  decreasePage:function(){
    this.pauseNavigation();
    showLoader();

    productPage-=1;
    this.getProducts();
  },

  setRowAnimations:function(){



    var animConfig = {"duration":1,"iterationCount":1,"delay":0,"fillMode":kony.anim.FORWARDS	};
    //scale
    var transformProp1 = kony.ui.makeAffineTransform();
    transformProp1.scale(0.0,0.0); 
    var transformProp2 = kony.ui.makeAffineTransform();
    transformProp2.scale(0.5,0.5);
    var transformProp3 = kony.ui.makeAffineTransform();
    transformProp3.scale(1,1);
    var animDefinitionOne = {0  : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp1},
                             // 50 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp2},
                             100 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp3}
                            } ;
    var animDefinition = kony.ui.createAnimation(animDefinitionOne);
    var finalAnimation = {definition: animDefinition, config: animConfig};
    this.view.segProductList.setAnimations({visible: finalAnimation});

  }




});