serviceName = "BestBuySystem";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);


define({ 

  onNavigate:function(){

    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderDefault.btnBack.onClick=this.getPreviousCategory;
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.view.HeaderDefault.btnMenu.onClick=openMenu.bind(this);
    this.closeAppMenu();
    this.setRowAnimations();
    this.view.segCategories.onRowClick=this.categoryClicked;
    this.view.searchTxt.onDone=this.inputSearch;
    this.view.HeaderDefault.btnSearch.onClick=this.openSearch;
    this.view.searchCancel.onClick=this.closeSearch;
    this.view.selectFilter.onSelection=this.selectFilter;
    this.view.flxShadow.onClick=this.doNothing();


    if(categoryCounter==0)
    {
      this.setLocales();
      this.setDefaultHome();
      this.getCategories();
      this.view.flxSearch.left="100%";
      this.view.flxCrumbs.top="0%";

    }else{

    }

  },doNothing:function(){

    kony.print("clicked overlay");
  },

  closeAppMenu:function(){


    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";


  },


  selectFilter:function(){

    inputFilter = this.view.selectFilter.selectedKeyValue[1];
  },
  inputSearch:function(){
    var searchInput = this.view.searchTxt.text.trim();

    if (inputFilter=="" || inputFilter=="<Select a value>"){

      if (searchInput=='' || searchInput==undefined){

        showError('Invalid Entry','Please type an item to search.');
      }else{
        searchValue=this.view.searchTxt.text;  
        queryValue = this.view.searchTxt.text;
        productPage=1;
        inputSearch=true;
        var ntf = new kony.mvc.Navigation('frmProductsList');
        ntf.navigate();

      }

    }else{

      if (searchInput=='' || searchInput==undefined){

        showError('Invalid Entry','Please type an item to search.');
      }else{
        queryValue = this.view.searchTxt.text+"&"+this.setFilterAttribute(inputFilter);
        searchValue=this.view.searchTxt.text;
        categoryPath=this.view.searchTxt.text;
        productPage=1;
        inputSearch=true;
        var ntf = new kony.mvc.Navigation('frmProductsList');
        ntf.navigate(); 

      }



    }


  },
  categoryClicked:function(){

    if (categoryCounter==2){
      for (var i=0; i<this.view.segCategories.selectedItems.length; i++){
        categorySelected = this.view.segCategories.selectedItems[i].id;
        queryValue = this.view.segCategories.selectedItems[i].category;
        searchValue = this.view.segCategories.selectedItems[i].category;
        productPage=1;
        inputSearch=false;
        this.pauseNavigation();
        showLoader();

        var ntf = new kony.mvc.Navigation('frmProductsList');
        ntf.navigate();

      } 

    }else{

      for (var i=0; i<this.view.segCategories.selectedItems.length; i++){
        categoryCounter+=1;

        categorySelected = this.view.segCategories.selectedItems[i].id;
        categoryNames.push(this.view.segCategories.selectedItems[i].category);

        categoryPath += ' -> ' + this.view.segCategories.selectedItems[i].category;

        this.pauseNavigation();
        showLoader();

        this.getSubCategories();
      } 

    }},


  getCategories:function(){

    this.pauseNavigation();
    showLoader();

    operationName =  "getCategories";
    data= {"id": "cat00000"};
    headers= {};
    integrationObj.invokeOperation(operationName, headers, data, operationSuccess.bind(this), operationFailure.bind(this));


    function operationSuccess(res){


      var Data = res.subCategories;


      const categoriesData = Data.map(item => {
        const categoriesInfo = {};

        categoriesInfo.category = item.name;
        categoriesInfo.id = item.id;
        return categoriesInfo;
      });

      cacheData.push(categoriesData);

      this.view.segCategories.widgetDataMap = {lblCategoryTitle : "category"};
      this.view.segCategories.setData(categoriesData);

      this.resumeNavigation();
      kony.application.dismissLoadingScreen();


    }
    function operationFailure(res){
      alert("Cannot run service.");
    }

  },


  getSubCategories:function(){

    operationName =  "getCategories";
    data= {"id": categorySelected};
    headers= {};
    integrationObj.invokeOperation(operationName, headers, data, operationSuccess.bind(this), operationFailure.bind(this));


    function operationSuccess(res){

      if (res.subCategories=='' || res.subCategories==undefined){

        showError("No Sub-Categories Found","We're sorry we found no sub-categories for this item.");
        categoryCounter-=1;
        categoryNames.pop();
        categoryPath=(categoryCounter>=1) ? `${kony.i18n.getLocalizedString("Home")} -> ${categoryNames}` :`${kony.i18n.getLocalizedString("Home")}` ;
        this.resumeNavigation();
        kony.application.dismissLoadingScreen();

      }

      else{

        var Data = res.subCategories;

        const categoriesData = Data.map(item => {
          const categoriesInfo = {};

          categoriesInfo.category = item.name;
          categoriesInfo.id = item.id;

          return categoriesInfo;
        });

        cacheData.push(categoriesData);

        this.view.segCategories.widgetDataMap = {lblCategoryTitle : "category"};
        this.view.segCategories.setData(categoriesData); 
        this.view.lblCrumbs.text=categoryPath;  

        this.view.HeaderDefault.imgBack.isVisible=true;
        this.view.HeaderDefault.btnBack.isVisible=true;

        this.resumeNavigation();
        kony.application.dismissLoadingScreen();

      }


    }


    function operationFailure(res){
      alert('no Data' +res);
    }

  },

  setDefaultHome:function(){
    categorySelected=[];
    categoryCounter=0;
    categoryPath=kony.i18n.getLocalizedString("Home");
    categoryNames=[];

    this.view.HeaderDefault.imgBack.isVisible=false;
    this.view.HeaderDefault.btnBack.isVisible=false;
    this.view.lblCrumbs.text=kony.i18n.getLocalizedString("Home");
    this.view.searchTxt.text='';
    this.view.selectFilter.selectedKey="default";



  },



  getPreviousCategory:function(){

    this.pauseNavigation();
    showLoader();

    categoryCounter-=1;
    cacheData.pop();
    categoryNames.pop();


    var index = categoryCounter;
    


    if (index==0)
    {

      categorySelected=[];
      categoryCounter=0;
      categoryPath=kony.i18n.getLocalizedString("Home");
      categoryNames=[];


      this.view.segCategories.widgetDataMap = {lblCategoryTitle : "category"};
      this.view.segCategories.setData(cacheData[index]); 

      this.view.HeaderDefault.imgBack.isVisible=false;
      this.view.HeaderDefault.btnBack.isVisible=false;
      this.view.lblCrumbs.text=kony.i18n.getLocalizedString("Home");



    }

    else

    {


      

      this.view.segCategories.widgetDataMap = {lblCategoryTitle : "category"};

      this.view.segCategories.setData(cacheData[index]); 
      this.view.lblCrumbs.text=`Home -> ${categoryNames[index-1]}`;

    }

    this.resumeNavigation();
    kony.application.dismissLoadingScreen();

  },

  setFilterAttribute:function(input){


    if (input=='On sale'){input='onSale=true';}
    if (input=='Not on sale'){input='onSale=false';}
    if (input=='New'){input='new=true';}
    if (input=='Not new'){input='new=false';}
    if (input=='Free Shipping'){input='free Shipping=true';}
    if (input=='No Free Shipping'){input='free Shipping=false';}


    return input;

  },

  setRowAnimations:function(){

    var animConfig = {"duration":1,"iterationCount":1,"delay":0,"fillMode":kony.anim.FORWARDS	};

    var transformProp1 = kony.ui.makeAffineTransform();
    transformProp1.scale(0.0,0.0); 

    var transformProp3 = kony.ui.makeAffineTransform();
    transformProp3.scale(1,1);
    var animDefinitionOne = {0  : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp1},

                             100 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp3}
                            } ;
    var animDefinition = kony.ui.createAnimation(animDefinitionOne);
    var finalAnimation = {definition: animDefinition, config: animConfig};
    this.view.segCategories.setAnimations({visible: finalAnimation});

  },
  setLocales:function(){

    this.view.searchTxt.text=kony.i18n.getLocalizedString("Search");
    this.view.lblCrumbs.text=kony.i18n.getLocalizedString("Home");

  },

  openSearch:function(){

    var self = this;


    self.view.flxSearch.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "0%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
      }, {

      });
    self.view.flxCrumbs.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "12%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
      }, {

      });

  },
  closeSearch:function(){

    var self = this;



    this.view.searchTxt.text="";

    self.view.flxSearch.animate(
      kony.ui.createAnimation({
        "100": {
          "left": "100%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
      }, {

      });
    self.view.flxCrumbs.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "0%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
      }, {

      });

  }




});