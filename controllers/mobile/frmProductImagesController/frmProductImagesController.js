define({ 

  onNavigate:function(){

    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderBack.btnMenu.onClick=openMenu.bind(this);
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.view.HeaderBack.btnBack.onClick=this.goBackProductDetail;
    this.closeAppMenu();
    this.getImages();
    this.view.selectedImage.onSelection=this.selectImage;
    
  },closeAppMenu:function(){
    
  
    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";
    
    
  },

  selectImage:function(){
    
     this.view.productImage.src = this.view.selectedImage.selectedKeyValue[0];
  },

  getImages:function(){





    if(selectedProduct[0].image_list=='' || selectedProduct[0].image_list==undefined){


      this.view.productImage.src=selectedProduct[0].image;

      var thumbnail=[

        selectedProduct[0].image,"Main Image",

      ];
      this.view.selectedImage.masterData=thumbnail;



    }else{

      this.view.productImage.src=selectedProduct[0].image;


      const imageData = selectedProduct[0].image_list.map(item => {

        var imageInfo = [
          item.href, this.setImageTitle(item.rel),

        ];


        return imageInfo;
      });


      this.view.selectedImage.masterData=imageData.sort();
    }




  },

  goBackProductDetail:function(){

    var ntf = new kony.mvc.Navigation('frmProductsDetail');
    ntf.navigate(selectedProduct[0]);
  },

  setImageTitle:function(title){

    if (title=='Front_Thumbnail'){title='Very Small';}

    if (title=='Angle_Large'){title='Large';}
    if (title=='Angle_Detail'){title='Small';}
    if (title=='Angle_Medium'){title='Medium';}
    if (title=='Front_Standard'){title='Front';}
    if (title=='Front_Zoom'){title='Front Zoom';}

    if (title=='Alt_View_Zoom_13'){title='Front 2';}
    if (title=='Alt_View_Zoom_14'){title='Front 3';}
    if (title=='Angle_Thumbnail'){title='Front 4';}
    if (title=='Angle_Standard'){title='Front 5';}
    if (title=='Alt_View_Standard_11' ){title='Angle Front 6';}

    if (title=='Front_Medium'){title='Front Medium';}
    if (title=='Front_Large'){title='Front Large';}
    if (title=='Front_Detail'){title='Front Detail';}
    if (title=='Alt_View_Zoom_11'){title='Front Detail Small';}

    if (title=='Angle_Zoom'){title='Angle Zoom';}


    if (title=='Alt_View_Zoom_15'){title='Angle Right';}
    if (title=='Alt_View_Zoom_12'){title='Angle Right Alt';}
    if (title=='Alt_View_Standard_13' ){title='Angle Right Zoom';}
    if (title=='Alt_View_Zoom_12'){title='Angle Right Detail';}
    if (title=='Alt_View_Zoom_1'){title='Angle Right Detail Zoom';}



    if (title=='Left_Standard'){title='Angle Left';}
    if (title=='Left_Zoom'){title='Angle Left Zoom';}
    if (title=='Alt_View_Standard_12'){title='Angle Left Detail';}     
    if (title=='Alt_View_Standard_1' ){title='Angle Back';}

    if (title=='Alt_View_Standard_16' ){title='Angle Back Zoom';}
    if (title=='Alt_View_Standard_14' ){title='Angle Back Detail';}
    if (title=='Alt_View_Zoom_16'){title='Angle Back Detail Zoom';}
    if (title=='Back_Standard'){title='Back';}


    if (title=='Alt_View_Standard_15' ){title='Box';}





    if (title=='Remote_Standard' || title=='Remote'){title='Remote';}
    if (title=='Remote_Zoom' || title=='Remote'){title='Remote Detail';}


    return title;

  }

});