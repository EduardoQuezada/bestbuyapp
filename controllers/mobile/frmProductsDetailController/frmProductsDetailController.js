define({ 

  onNavigate:function(product){


    this.view.Menu.btnHome.onClick=navigateHome;
    this.view.Menu.btnStores.onClick=navigateStoreLocator;
    this.view.Menu.btnCart.onClick=navigateCart;
    this.view.HeaderBack.btnMenu.onClick=openMenu.bind(this);
    this.view.Menu.flxOverlay.onClick=closeMenu.bind(this);
    this.view.btnOpen.onClick=this.openReviews;
    this.view.btnClose.onClick=this.closeReviews;
    this.view.btnImages.onClick=(product.image=='no_image.png') ? this.noImages : this.navigateProductImages;
    this.view.HeaderBack.btnBack.onClick=navigateProducts;
    this.view.btnAdd.onClick=this.addToCart;
    this.setProductData(product);
    this.getProductReviews(product);
    this.setTimeOut();
    this.closeAppMenu();
    this.resetReviews();


  },resetReviews:function(){

    this.view.flxReviewContainer.top="100%";
    this.view.flxBtn.top="75%";
    this.view.flxOpenBtn.isVisible=true;
    this.view.flxCloseBtn.isVisible=false;

  },closeAppMenu:function(){


    this.view.Menu.left="-100%";
    this.view.flxMain.left="0%";


  },noImages:function(){
    showError("No Images Found","We could not find any images for this product.");

  },

  setTimeOut: function(){

    kony.application.registerForIdleTimeout(2, navigateHome);
  },

  setProductData:function(product){
    selectedProduct=[];
    selectedProduct.push(product);
    this.view.imgProduct.src=product.image;
    this.view.lblTitle.text=product.title;



    this.view.lblPrice.text= ( product.sale == 'true') ? `On Sale! $${product.price.text}`:`$${product.price.text}`;
    this.view.lblPrice.skin= product.price.skin;
    this.view.lblReviews.text=(product.review_average=='' || product.review_average==undefined ||  product.review_average==0 ) ? `Avg review: N/A` :`Avg review: ${product.review_average}`;
    this.view.imgRating.src=this.setReview(product.review_average);
    this.view.lblDesc.text=product.desc;



  },
  getProductReviews:function(product){

    operationName =  "getReviews";
    data= {"sku": product.sku};
    headers= {};
    integrationObj.invokeOperation(operationName, headers, data, operationSuccess.bind(this), operationFailure.bind(this));
    function operationSuccess(res){


      if (res.reviews == undefined || res.reviews == ''){

        this.view.lblTotalReviews.text=`No Reviews`;
        this.view.segUserReviews.isVisible=false;
        this.view.flxBtn.isVisible=false;

        this.resumeNavigation();
        kony.application.dismissLoadingScreen();
      }else{



        var Data = res.reviews;


        const productsReviewsData = Data.map(item => {
          const productsReview = {};

          productsReview.title = item.title;
          productsReview.user = `Reviewer: ${item.name}`;
          productsReview.imgRating=this.setReview(item.rating);
          productsReview.comment = item.comment;

          return productsReview;
        });

        this.view.segUserReviews.widgetDataMap = {revTitle : "title", revSubmit: "user", revRating : "imgRating", revDesc:"comment"};
        this.view.segUserReviews.setData(productsReviewsData);

        this.view.lblTotalReviews.text=`Total Number Of Reviews: ${product.reviewCount}`;
        this.view.segUserReviews.isVisible=true;
        this.view.flxBtn.isVisible=true;

        this.resumeNavigation();
        kony.application.dismissLoadingScreen();
      }


    }
    function operationFailure(res){
      alert("Failure to get Product Reviews.");
    }



  },
  navigateProductImages:function(){
    var ntf = new kony.mvc.Navigation('frmProductImages');
    ntf.navigate();

  },
  addToCart:function(){



    shoppingCart.push(selectedProduct[0]);


    totalCartValue = totalCartValue + parseFloat(selectedProduct[0].price.text);

    this.showItemAdded();


  },
  setReview:function(count){
    var ratingImage;

    if (count>=1 && count< 2 ){ratingImage="ratings_star_1.png";}
    if (count>=2 && count< 3 ){ratingImage="ratings_star_2.png";}
    if (count>=3 && count< 4 ){ratingImage="ratings_star_3.png";}
    if (count>=4 && count< 4.5 ){ratingImage="ratings_star_4.png";}
    if (count>=4.5){ratingImage="ratings_star_5.png";}

    return ratingImage;
  },

  openReviews:function(){

    var self = this;

    function enableCloseBtn() {
      self.view.flxOpenBtn.isVisible = false;
      self.view.flxCloseBtn.isVisible = true;
    }

    self.view.flxReviewContainer.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "21.46%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 2
      }, {

      });
    self.view.flxBtn.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "0%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 2
      }, {
        "animationEnd": enableCloseBtn()
      });
  },

  closeReviews:function(){

    var self = this;

    function showOpenBtn() {
      self.view.flxCloseBtn.isVisible = false;
      self.view.flxOpenBtn.isVisible = true;
    }

    self.view.flxReviewContainer.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "100%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 2
      }, {

      });
    self.view.flxBtn.animate(
      kony.ui.createAnimation({
        "100": {
          "top": "75%",
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          }
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 2
      }, {
        "animationEnd": showOpenBtn()
      });
  },

  showItemAdded:function(){

    kony.ui.Alert({
      "alertType": constants.ALERT_TYPE_ERROR,
      "alertTitle": "Item Added",
      "yesLabel": "Ok",
      "alertIcon": "item_added.png",
      "message": "This item has been added to the shopping cart.",

    }, {
      "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
  }


});