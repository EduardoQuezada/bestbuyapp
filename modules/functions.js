currency = new Intl.NumberFormat('en-US', {style: 'currency',currency: 'USD',maximumFractionDigits: 2});

var cacheData=[];
var categorySelected="cat00000";
var categoryNames=[];
var categoryPath="Home";
var categoryCounter=0;
var selectedProduct=[];
var selectedImages=[];

var inputSearch=false;
var productPage=1;
var inputFilter="";
var searchValue="";

var shoppingCart=[];
var totalCartValue=0;

function showLoader(){
  
  if (kony.os.deviceInfo().name =='iphone'){
    
      kony.application.showLoadingScreen(null, "Loading...", 
  constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {  
  shouldShowLabelInBottom: "true", separatorHeight: 30} );
    
  }else{
        kony.application.showLoadingScreen(null, "Loading...", 
  constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {  
  shouldShowLabelInBottom: "true", separatorHeight: 30} );
    
  }


}



function navigateHome(){
categoryCounter=0;
closeMenu.bind(this);
var ntf = new kony.mvc.Navigation('frmHome');
ntf.navigate();
}

function navigateStoreLocator(){

var ntf = new kony.mvc.Navigation('frmStoreLocator');
ntf.navigate();
}

function navigateCart(){

var ntf = new kony.mvc.Navigation('frmShopCart');
ntf.navigate();
}

function navigateProducts(){
  
   var ntf = new kony.mvc.Navigation('frmProductsList');
ntf.navigate(categorySelected);
  
}
function showError(title,msg){

    kony.ui.Alert({
      "alertType": constants.ALERT_TYPE_ERROR,
      "alertTitle": title,
      "yesLabel": "Ok",
      "alertIcon": "error.png",
      "message": msg,

    }, {
      "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
  }

